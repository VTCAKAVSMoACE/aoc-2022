use std::collections::HashSet;
use std::fmt::{Debug, Display, Formatter};

const INPUT: &'static str = include_str!("../inputs/day-9");

#[derive(Debug, Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl From<&str> for Direction {
    fn from(value: &str) -> Self {
        match value.trim() {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            _ => unimplemented!(),
        }
    }
}

#[derive(Debug, Default, Copy, Clone)]
struct Knot {
    head: (isize, isize),
    tail: (isize, isize),
}

#[derive(Debug, Copy, Clone)]
struct Rope<const KNOTS: usize> {
    knots: [Knot; KNOTS],
}

impl Knot {
    fn new() -> Self {
        Self::default()
    }

    fn perform_tail_follow(&mut self) {
        let mut delta_x = self.head.0 - self.tail.0;
        let mut delta_y = self.head.1 - self.tail.1;
        let abs_x = delta_x.abs();
        let abs_y = delta_y.abs();
        assert!(abs_x <= 2, "expected {} <= 2", abs_x);
        assert!(abs_y <= 2, "expected {} <= 2", abs_y);
        if abs_x == 2 {
            if abs_y == 1 {
                self.tail = self.head;
                delta_x = -delta_x;
                delta_y = -delta_y;
            }
            self.tail.0 += delta_x / 2;
            self.tail.1 += delta_y / 2;
        } else if abs_y == 2 {
            if abs_x == 1 {
                self.tail = self.head;
                delta_x = -delta_x;
                delta_y = -delta_y;
            }
            self.tail.0 += delta_x / 2;
            self.tail.1 += delta_y / 2;
        }
    }

    fn move_head(&mut self, dir: Direction) {
        match dir {
            Direction::Up => {
                self.head.1 += 1;
            }
            Direction::Down => {
                self.head.1 -= 1;
            }
            Direction::Left => {
                self.head.0 -= 1;
            }
            Direction::Right => {
                self.head.0 += 1;
            }
        }
        self.perform_tail_follow();
    }

    fn move_head_to(&mut self, pos: (isize, isize)) {
        self.head = pos;
        self.perform_tail_follow();
    }
}

impl<const KNOTS: usize> Rope<KNOTS> {
    fn new() -> Self {
        Self {
            knots: [Knot::new(); KNOTS],
        }
    }

    fn move_head(&mut self, dir: Direction) {
        self.knots[0].move_head(dir);
        for i in 1..KNOTS {
            self.knots[i].move_head_to(self.knots[i - 1].tail);
        }
    }

    fn head(&self) -> (isize, isize) {
        self.knots.first().unwrap().head
    }

    fn tail(&self) -> (isize, isize) {
        self.knots.last().unwrap().tail
    }
}

fn simulate<const KNOTS: usize>(mut rope: Rope<KNOTS>) -> HashSet<(isize, isize)> {
    let mut visited = HashSet::new();
    for line in INPUT.lines() {
        let (inst, count) = line.split_at(2);
        let dir = Direction::from(inst);
        let count: usize = count.parse().unwrap();
        for _ in 0..count {
            rope.move_head(dir);
            visited.insert(rope.tail());
        }
    }
    visited
}

impl<const KNOTS: usize> Display for Rope<KNOTS> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.head())?;
        for knot in self.knots {
            write!(f, ", {:?}", knot.tail)?;
        }
        Ok(())
    }
}

fn main() {
    let single = Rope::<1>::new();
    println!("visited (single): {}", simulate(single).len());
    let many = Rope::<9>::new();
    println!("visited (many): {}", simulate(many).len());
}
