use std::str::FromStr;

const INPUT: &'static str = include_str!("../inputs/day-1");

fn main() {
    let mut elves = INPUT
        .split("\n\n")
        .map(|elf| {
            elf.lines()
                .map(|cals| usize::from_str(cals).expect("Invalid character present."))
                .sum::<usize>()
        })
        .enumerate()
        .collect::<Vec<_>>();
    elves.sort_by_key(|elf| !elf.1);

    println!("{:?}", elves);
}
