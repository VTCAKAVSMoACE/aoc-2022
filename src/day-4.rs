#![no_std]

extern crate std;

use core::ops::RangeInclusive;
use core::str::FromStr;
use std::println;

const INPUT: &'static str = include_str!("../inputs/day-4");

fn main() {
    println!(
        "fully overlapped: {}",
        INPUT
            .lines()
            .map(|line| {
                let mut iter = line.split(',').map(|range| {
                    let mut iter = range.split('-').map(|idx| usize::from_str(idx));
                    RangeInclusive::new(
                        iter.next().unwrap().unwrap(),
                        iter.next().unwrap().unwrap(),
                    )
                });
                (iter.next().unwrap(), iter.next().unwrap())
            })
            .filter(|(range1, range2)| {
                (range1.start() <= range2.start() && range1.end() >= range2.end())
                    || (range2.start() <= range1.start() && range2.end() >= range1.end())
            })
            .count()
    );
    println!(
        "overlapped: {}",
        INPUT
            .lines()
            .map(|line| {
                let mut iter = line.split(',').map(|range| {
                    let mut iter = range.split('-').map(|idx| usize::from_str(idx));
                    RangeInclusive::new(
                        iter.next().unwrap().unwrap(),
                        iter.next().unwrap().unwrap(),
                    )
                });
                (iter.next().unwrap(), iter.next().unwrap())
            })
            .filter(|(range1, range2)| {
                (range1.contains(range2.start()) || range1.contains(range2.end()))
                    || (range2.contains(range1.start()) || range2.contains(range1.end()))
            })
            .count()
    );
}
