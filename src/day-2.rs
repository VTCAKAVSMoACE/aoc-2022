#![no_std]

extern crate std;

use std::println;

const INPUT: &'static str = include_str!("../inputs/day-2");

#[derive(Debug, Copy, Clone)]
#[repr(usize)]
enum Selection {
    Rock = 0,
    Paper = 1,
    Scissors = 2,
}

#[derive(Debug, Copy, Clone)]
#[repr(usize)]
enum RoundResult {
    Loss = 0,
    Tie = 3,
    Win = 6,
}

impl From<u8> for Selection {
    fn from(value: u8) -> Self {
        match value {
            b'A' | b'X' => Selection::Rock,
            b'B' | b'Y' => Selection::Paper,
            b'C' | b'Z' => Selection::Scissors,
            _ => unimplemented!(),
        }
    }
}

impl From<u8> for RoundResult {
    fn from(value: u8) -> Self {
        match value {
            b'X' => RoundResult::Loss,
            b'Y' => RoundResult::Tie,
            b'Z' => RoundResult::Win,
            _ => unimplemented!(),
        }
    }
}

// (0, 0) => 3
// (0, 1) => 0
// (0, 2) => 6
// (1, 0) => 6
// (1, 1) => 3
// (1, 2) => 0
// (2, 0) => 0
// (2, 1) => 6
// (2, 2) => 3
//
// res = ((first + 4 - second) % 3) * 3
// res / 3 = (first + 4 - second) % 3
// (res / 3) % 3 = (first + 4 - second) % 3 // assumption about res/3 here is sound
// (first) % 3 = (res / 3 + second - 4) % 3
// first % 3 = (res / 3 + second + 2) % 3
fn main() {
    println!(
        "first total score: {}",
        INPUT
            .lines()
            .map(|line: &str| line.as_bytes())
            .map(|round| (Selection::from(round[0]), Selection::from(round[2])))
            .map(|(other, me)| ((me as usize + 4 - other as usize) % 3) * 3 + me as usize + 1)
            .sum::<usize>()
    );
    println!(
        "second total score: {}",
        INPUT
            .lines()
            .map(|line: &str| line.as_bytes())
            .map(|round| (Selection::from(round[0]), RoundResult::from(round[2])))
            .map(|(other, result)| result as usize
                + (result as usize / 3 + other as usize + 2) % 3
                + 1)
            .sum::<usize>()
    );
}
