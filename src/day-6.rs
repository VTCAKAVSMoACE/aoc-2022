#![no_std]
#![feature(array_windows)]

extern crate std;

use std::println;

const INPUT: &'static str = include_str!("../inputs/day-6");

fn start_detect<const N: usize>() -> usize {
    INPUT
        .as_bytes()
        .array_windows::<N>()
        .enumerate()
        .find(|(_, window)| !(1..window.len()).any(|i| window[i..].contains(&window[i - 1])))
        .unwrap()
        .0
        + N
}

fn main() {
    println!("start-of-packet:  {}", start_detect::<4>());
    println!("start-of-message: {}", start_detect::<14>());
}
