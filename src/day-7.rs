use std::collections::HashMap;
use std::path::{Path, PathBuf};

const INPUT: &'static str = include_str!("../inputs/day-7");

#[derive(Debug)]
enum DirEntry {
    Dir {
        entries: HashMap<&'static str, DirEntry>,
    },
    File {
        size: usize,
    },
}

impl DirEntry {
    fn dirs_with_size_matching<F: Fn(usize) -> bool>(
        &self,
        path: &mut PathBuf,
        results: &mut Vec<(PathBuf, usize)>,
        pred: &F,
    ) -> usize {
        match self {
            DirEntry::Dir { entries } => {
                let mut size = 0;
                for (name, entry) in entries {
                    path.push(name);
                    size += entry.dirs_with_size_matching(path, results, pred);
                    path.pop();
                }
                if pred(size) {
                    results.push((path.clone(), size))
                }
                size
            }
            DirEntry::File { size } => *size,
        }
    }
}

fn main() {
    let mut stack = Vec::new();
    let root = DirEntry::Dir {
        entries: HashMap::new(),
    };
    let mut lines = INPUT.lines();
    lines.next();
    stack.push(("/", root));
    for line in lines {
        if line.starts_with("$ ") {
            if line == "$ ls" {
                continue;
            }
            if line == "$ cd .." {
                let (name, entry) = stack.pop().unwrap();
                let Some((_, DirEntry::Dir { entries, .. })) = stack.last_mut() else { unreachable!() };
                entries.insert(name, entry);
                continue;
            }

            // traversing into dir
            let (_, name) = line.split_at(5);
            let Some((_, DirEntry::Dir { entries, .. })) = stack.last_mut() else { unreachable!() };
            let target = entries.remove(name).unwrap();
            stack.push((name, target));
        } else if line.starts_with("dir ") {
            let (_, name) = line.split_at(4);
            let Some((_, DirEntry::Dir { entries, .. })) = stack.last_mut() else { unreachable!() };
            entries.insert(
                name,
                DirEntry::Dir {
                    entries: HashMap::new(),
                },
            );
        } else {
            let (size, name) = line.split_once(" ").unwrap();
            let size = size.parse().unwrap();
            let Some((_, DirEntry::Dir { entries, .. })) = stack.last_mut() else { unreachable!() };
            entries.insert(name, DirEntry::File { size });
        }
    }
    let root = loop {
        let (name, entry) = stack.pop().unwrap();
        if let Some((_, DirEntry::Dir { entries, .. })) = stack.last_mut() {
            entries.insert(name, entry);
        } else {
            break entry;
        }
    };

    let mut path = PathBuf::new();
    let mut paths = Vec::new();

    let used = root.dirs_with_size_matching(&mut path, &mut paths, &|size| size < 100000);
    let mut total = 0;
    for (dir, size) in paths.iter() {
        println!("{}: {}", dir.to_str().unwrap(), size);
        total += *size;
    }
    println!("total: {}", total);

    path.clear();
    paths.clear();

    let needed = used - (70000000 - 30000000);
    root.dirs_with_size_matching(&mut path, &mut paths, &|size| size >= needed);
    paths.sort_unstable_by_key(|(dir, size)| *size);
    let candidate = paths.first().unwrap();
    println!(
        "best candidate for deletion: {} ({})",
        candidate.0.to_str().unwrap(),
        candidate.1
    );
}
