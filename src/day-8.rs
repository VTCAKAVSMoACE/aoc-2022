#![feature(iter_next_chunk)]

use std::collections::HashSet;

const INPUT: &'static str = include_str!("../inputs/day-8");
const LEN: usize = {
    let mut target = 0;
    loop {
        if INPUT.as_bytes()[target] == b'\n' {
            break target;
        }
        target += 1;
    }
};

fn columns_iter<F: Fn(usize) -> usize>(idx_transformer: F) -> impl Iterator<Item = [u8; LEN]> {
    let mut column_iter = (0..LEN)
        .map(move |idx| idx_transformer(idx))
        .flat_map(|idx| INPUT.lines().map(move |line| line.as_bytes()[idx]));

    std::iter::from_fn(move || column_iter.next_chunk().ok())
}

fn compute_visible(mut lines: impl Iterator<Item = [u8; LEN]>) -> Vec<Vec<(usize, u8)>> {
    let mut visible = lines
        .next()
        .unwrap()
        .into_iter()
        .map(|tree| vec![(0usize, tree)])
        .collect::<Vec<_>>();
    for (line_idx, line) in (1..).zip(lines) {
        for (tree_idx, tree) in line.into_iter().enumerate() {
            let Some((_, last_visible)) = visible[tree_idx].last().copied() else { unreachable!() };
            if last_visible < tree {
                visible[tree_idx].push((line_idx, tree));
            }
        }
    }
    visible
}

fn part_1() {
    let top_visible = compute_visible(
        INPUT
            .lines()
            .map(|line| <&[u8; LEN]>::try_from(line.as_bytes()).copied().unwrap()),
    );
    let bottom_visible = compute_visible(
        INPUT
            .lines()
            .rev()
            .map(|line| <&[u8; LEN]>::try_from(line.as_bytes()).copied().unwrap()),
    );
    let left_visible = compute_visible(columns_iter(|i| i));
    let right_visible = compute_visible(columns_iter(|i| LEN - i - 1));

    let mut all_visible = HashSet::new();
    for (c, line) in top_visible.into_iter().enumerate() {
        for (r, _) in line {
            all_visible.insert((r, c));
        }
    }
    for (c, line) in bottom_visible.into_iter().enumerate() {
        for (r, _) in line {
            let r = LEN - r - 1; // flip row order
            all_visible.insert((r, c));
        }
    }
    for (r, line) in left_visible.into_iter().enumerate() {
        for (c, _) in line {
            all_visible.insert((r, c));
        }
    }
    for (r, line) in right_visible.into_iter().enumerate() {
        for (c, _) in line {
            let c = LEN - c - 1; // flip column order
            all_visible.insert((r, c));
        }
    }

    println!("visible: {}", all_visible.len());
}

fn compute_sight_map(lines: impl Iterator<Item = [u8; LEN]>) -> Box<[[u8; LEN]; LEN]> {
    let map = lines.collect::<Vec<_>>(); // sorry nathan
    let mut sight_lines = Box::new([[0u8; LEN]; LEN]);

    for (row, line) in map.iter().enumerate().skip(1) {
        let mut sight_line = [1; LEN];
        for (column, tree) in line.into_iter().copied().enumerate() {
            let sight = &mut sight_line[column];
            loop {
                match row.checked_sub(*sight as usize) {
                    Some(next_idx) if next_idx > 0 => {
                        let next = map[next_idx][column];
                        if next < tree {
                            // if we see over this tree, we see all of the trees it sees
                            *sight += sight_lines[next_idx][column];
                        } else {
                            break;
                        }
                    }
                    _ => {
                        break;
                    }
                }
            }
        }
        sight_lines[row].copy_from_slice(&sight_line);
    }

    sight_lines
}

#[allow(unused)]
fn print_sight_line(sight_line: &[[u8; LEN]; LEN]) {
    for row in sight_line.into_iter() {
        for entry in row {
            print!("{: >3}", entry);
        }
        println!()
    }
}

fn part_2() {
    let up_sight_lines = compute_sight_map(
        INPUT
            .lines()
            .map(|line| <&[u8; LEN]>::try_from(line.as_bytes()).copied().unwrap()),
    );
    let down_sight_lines = compute_sight_map(
        INPUT
            .lines()
            .rev()
            .map(|line| <&[u8; LEN]>::try_from(line.as_bytes()).copied().unwrap()),
    );
    let left_sight_lines = compute_sight_map(columns_iter(|i| i));
    let right_sight_lines = compute_sight_map(columns_iter(|i| LEN - i - 1));
    let mut best_score = 0;
    for row in 1..(LEN - 1) {
        for col in 1..(LEN - 1) {
            let up = up_sight_lines[row][col] as usize;
            let down = down_sight_lines[LEN - row - 1][col] as usize;
            let left = left_sight_lines[col][row] as usize;
            let right = right_sight_lines[LEN - col - 1][row] as usize;
            let score = up * down * left * right;
            if score > best_score {
                best_score = score;
            }
        }
    }
    println!("best scenic score: {}", best_score);
}

fn main() {
    part_1();
    part_2();
}
