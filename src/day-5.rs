use std::collections::BTreeMap;

const INPUT: &'static str = include_str!("../inputs/day-5");

fn part_1() {
    let mut lines = INPUT.lines();
    let mut containers = BTreeMap::new();
    while let Some(line) = lines.next() {
        let line = line.as_bytes();
        if line[1] != b' ' && line[0] == b' ' {
            break;
        }

        for (i, container) in line.chunks(4).enumerate() {
            let name = container[1];
            if !name.is_ascii_whitespace() {
                containers.entry(i + 1).or_insert_with(Vec::new).push(name);
            }
        }
    }
    containers.values_mut().for_each(|stack| stack.reverse());
    lines.next();
    for line in lines {
        let mut words = line.split_ascii_whitespace();
        if words.next().is_none() {
            break;
        }
        let amount = words.next().unwrap().parse::<usize>().unwrap();
        words.next();
        let from = words.next().unwrap().parse::<usize>().unwrap();
        words.next();
        let to = words.next().unwrap().parse::<usize>().unwrap();
        let mut from_stack = containers.remove(&from).unwrap_or_default();
        let mut to_stack = containers.remove(&to).unwrap_or_default();
        for _ in 0..amount {
            to_stack.push(from_stack.pop().unwrap());
        }
        containers.insert(from, from_stack);
        containers.insert(to, to_stack);
    }
    println!(
        "cratemover 9000: {}",
        containers
            .values()
            .filter_map(|stack| stack.last())
            .map(|&top| char::from(top))
            .collect::<String>()
    );
}

fn part_2() {
    let mut lines = INPUT.lines();
    let mut containers = BTreeMap::new();
    while let Some(line) = lines.next() {
        let line = line.as_bytes();
        if line[1] != b' ' && line[0] == b' ' {
            break;
        }

        for (i, container) in line.chunks(4).enumerate() {
            let name = container[1];
            if !name.is_ascii_whitespace() {
                containers.entry(i + 1).or_insert_with(Vec::new).push(name);
            }
        }
    }
    containers.values_mut().for_each(|stack| stack.reverse());
    lines.next();
    for line in lines {
        let mut words = line.split_ascii_whitespace();
        if words.next().is_none() {
            break;
        }
        let amount = words.next().unwrap().parse::<usize>().unwrap();
        words.next();
        let from = words.next().unwrap().parse::<usize>().unwrap();
        words.next();
        let to = words.next().unwrap().parse::<usize>().unwrap();
        let mut from_stack = containers.remove(&from).unwrap_or_default();
        let mut to_stack = containers.remove(&to).unwrap_or_default();
        let end = from_stack.len() - amount;
        to_stack.extend_from_slice(&from_stack[end..]);
        from_stack.truncate(end);
        containers.insert(from, from_stack);
        containers.insert(to, to_stack);
    }
    println!(
        "cratemover 9001: {}",
        containers
            .values()
            .filter_map(|stack| stack.last())
            .map(|&top| char::from(top))
            .collect::<String>()
    );
}

fn main() {
    part_1();
    part_2();
}
