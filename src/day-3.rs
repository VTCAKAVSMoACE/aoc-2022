#![no_std]

extern crate std;

use std::println;

const INPUT: &'static str = include_str!("../inputs/day-3");

fn main() {
    println!(
        "first priority: {}",
        INPUT
            .lines()
            .map(|line| line.split_at(line.len() / 2))
            .filter_map(|(first, second)| first.bytes().find(|c| second.as_bytes().contains(c)))
            .map(|b| match b {
                lower @ b'a'..=b'z' => lower - b'a' + 1,
                upper @ b'A'..=b'Z' => upper - b'A' + 27,
                _ => unimplemented!(),
            })
            .map(|priority| priority as usize)
            .sum::<usize>()
    );
    let mut step = 0;
    let mut buf = [INPUT, INPUT];
    println!(
        "second priority: {}",
        INPUT
            .lines()
            .filter_map(|line| {
                if step < buf.len() {
                    buf[step] = line;
                    step += 1;
                    None
                } else {
                    step = 0;
                    line.bytes()
                        .find(|c| buf[0].as_bytes().contains(c) && buf[1].as_bytes().contains(c))
                }
            })
            .map(|b| match b {
                lower @ b'a'..=b'z' => lower - b'a' + 1,
                upper @ b'A'..=b'Z' => upper - b'A' + 27,
                _ => unimplemented!(),
            })
            .map(|priority| priority as usize)
            .sum::<usize>()
    );
}
